import time
from typing import List

from click import echo_via_pager, progressbar
from jikanpy import Jikan as JikanClient
from jikanpy.exceptions import APIException
from tabulate import tabulate

from animefinder.cache import AnimeCache
from animefinder.cmd import Command, CommandHandler, Option, extract_optional_args
from animefinder.model import (
    GENRES_MAP,
    WATCHING_STATUS,
    AnimeGeneric,
    AnimeInGenre,
    AnimeInList,
    AnimeInSeason,
    AnimeSearch,
)
from animefinder.utils import (
    HEX_CYAN,
    NO_STYLE,
    cleanup_str,
    clear_screen,
    custom_print,
    error_print,
    success_print,
    warning_print,
)


class AnimeFinder:

    USER_NOT_SET = "NOT SET"

    # Additional mapping to make the typing easier (e.g. ptw vs Plan to Watch)
    LIST_STATUS_MAP = {"watching": 1, "completed": 2, "onhold": 3, "dropped": 4, "ptw": 6}

    def __init__(self, host: str, cache_path: str = None):
        if host:
            self.api: JikanClient = JikanClient(selected_base=host)
        else:
            self.api: JikanClient = JikanClient()

        self.mal_user: str = self.USER_NOT_SET
        self.mal_list: List[AnimeInList] = []
        self.cache = AnimeCache(cache_path=cache_path)

        self.handler = CommandHandler(not_found="Unknown command. Type 'help' to see usage information.")
        self.register_commands()
        warning_print("Welcome to AnimeFinder! Type 'help' to get started.")

    def terminate(self):
        warning_print("Saving cache...")
        self.cache.save()
        success_print("Goodbye!")

    def register_commands(self):
        connect = Command(
            "connect",
            usage="connect <mal_username>",
            args=True,
            description="Retrieve user's anime list from MAL",
            func=self.connect_mal,
        )

        list_cmd = Command(
            "list", usage="list", args=False, description="Show anime list (MAL)", func=self.display_list
        )
        list_cmd.add_option(
            Option(
                "status",
                "watching status to include - comma separated list. "
                f"Values accepted: {', '.join(self.LIST_STATUS_MAP.keys())}",
            )
        )
        list_cmd.add_option(Option("exclude", f"exclude genre - Genres: {', '.join(sorted(GENRES_MAP.values()))}"))

        search = Command(
            "search",
            usage="search <anime_name>",
            args=True,
            description="Will search in your list first then on MAL",
            func=self.search_anime,
        )

        get = Command(
            "get",
            usage="get <anime_id>",
            args=True,
            description="Retrieve anime full details (from MAL)",
            func=self.get_anime,
        )

        help_cmd = Command("help", usage="help", args=False, description="List all commands", func=self.display_help)

        cls = Command("cls", usage="cls", args=False, description="Clears the screen", func=clear_screen)

        season = Command(
            "season",
            usage="season <year> <summer|spring|fall|winter>",
            args=True,
            description="Shows all anime in given season/year",
            func=self.search_season,
        )

        stats = Command(
            "stats", usage="stats", args=False, description="Display stats from anime list", func=self.display_stats
        )

        genre = Command(
            "genre",
            usage="genre <anime_genre>",
            args=True,
            func=self.display_genres,
            description="List anime by genre\n\nGenres: %s" % ", ".join(sorted(GENRES_MAP.values())),
        )
        genre.add_option(Option("page", "page number"))

        self.handler.register(connect)
        self.handler.register(list_cmd)
        self.handler.register(search)
        self.handler.register(get)
        self.handler.register(help_cmd)
        self.handler.register(cls)
        self.handler.register(season)
        self.handler.register(stats)
        self.handler.register(genre)

    def _fetch_generic_anime(self, anime_id: int, max_days=30) -> AnimeGeneric:
        generic = self.cache.get(anime_id, max_days)
        if generic is None:
            try:
                error_print(f"Fetching anime {anime_id}...")
                raw = self.api.anime(anime_id)
            except APIException as err:
                raise err
            else:
                generic = AnimeGeneric(**raw)
                self.cache.update(anime_id, generic)

                time.sleep(0.1)  # to avoid rate limit
        return generic

    def display_help(self):
        """Display the commands help"""
        print("Available commands:")
        for cmd in sorted(self.handler.commands.keys()):
            short_desc = self.handler.commands[cmd].desc.split("\n\n")[0]
            print(f"  {cmd} - {short_desc}")
        print("\nTo print usage info about a specific command, run '<command> --help'\n")

    def connect_mal(self, username: str):
        """Attempts to retrieve given user's anime list from MAL"""
        print("Retrieving anime list from MAL...")
        try:
            response = self.api.user(username=username, request="animelist")
            self.mal_list = [AnimeInList(**raw) for raw in response["anime"]]
            self.mal_user = username
            success_print(f"Anime list successfully retrieved ({self.mal_user}).")
        except APIException as err:
            if str(err).startswith("400"):
                error_print(f"User '{username}' not found")
            else:
                error_print(f"Error retrieving list: {err}")

    def display_genres(self, args):
        if type(args) == str:
            args = args.split()

        if str(args[0]).lower() not in GENRES_MAP.values():
            error_print("Unknown genre")
            return

        genre = str(args[0]).lower()
        for genre_id, name in GENRES_MAP.items():
            if name == genre:
                target = genre_id
                break
        else:
            error_print(f"No ID found for genre: {genre}")
            return

        options = extract_optional_args(args)
        for name, value in options:
            if name == "page":
                try:
                    page = int(value)
                    break
                except ValueError:
                    error_print("'page' must be an integer")
                    return
        else:
            page = 1

        try:
            response = self.api.genre(type="anime", genre_id=target, page=page)
        except APIException as err:
            error_print(f"Error fetching genre {genre}: {err} ")
            return

        total = response.get("item_count")
        page_results = [AnimeInGenre(**raw) for raw in response.get("anime")]
        success_print(f"Page #{page} count: {len(page_results)} (total is {total})")

        working_list = []
        for anime in page_results:
            working_list.append(
                (
                    {
                        "title": anime.title,
                        "episodes": anime.episodes or "-",
                        "score": anime.score or 0.0,
                        "genres": ", ".join([genre.name for genre in anime.genres]),
                        "mal_id": anime.mal_id,
                    }
                )
            )

        print(f"Anime List by genre: {genre} \n---")
        input("Press ENTER to view results ('q' to return)...")

        # Sort by score, then display in table/pager
        echo_via_pager(
            tabulate(
                sorted(working_list, key=lambda k: k["score"], reverse=True),
                headers="keys",
                tablefmt="github",
                stralign="left",
                numalign="left",
            )
        )
        print()

    def display_stats(self):
        if not self.mal_list:
            self.display_empty_list()
            return

        # Compute stats from animelist + direct requests for each (necessary to get genres info)
        completed = watching = total_ep = 0
        genres_ep: dict = {i: 0 for i in GENRES_MAP}
        genres_avg: dict = {i: 0 for i in GENRES_MAP}
        warning_print("Retrieving stats for every anime in the list. Please wait...")
        with progressbar(self.mal_list) as animelist:
            anime: AnimeInList
            for anime in animelist:
                if WATCHING_STATUS[anime.watching_status] == "Watching":
                    watching += 1
                elif WATCHING_STATUS[anime.watching_status] == "Completed":
                    completed += 1

                if anime.watched_episodes:
                    total_ep += anime.watched_episodes

                    # we need the 'generic' anime object from main endpoint, to get the genres info
                    # try to hit the cache first (last 30 days), fetch again otherwise
                    generic = self.cache.get(anime.mal_id, max_days=30)
                    if generic is None:
                        try:
                            raw = self.api.anime(anime.mal_id)
                        except APIException as err:
                            error_print(f"Skipped anime {anime.mal_id} - {err}")
                            continue
                        else:
                            generic = AnimeGeneric(**raw)
                            self.cache.update(anime.mal_id, generic)

                            # to avoid rate limit
                            time.sleep(0.1)

                    genre_ids = [genre.mal_id for genre in generic.genres]
                    for i in genre_ids:
                        try:
                            genres_ep[i] += anime.watched_episodes
                        except KeyError:
                            warning_print(f"Unknown anime genre found (id: {i}). Ignoring for stats.")
                            continue

                        if anime.score:
                            genres_avg[i] = (genres_avg[i] + anime.score) / 2.0

        # most watched (comparing total ep watched per genre), top rated (comparing avg score)
        mw = sorted(genres_ep.items(), key=lambda item: item[1], reverse=True)
        tr = sorted(genres_avg.items(), key=lambda item: item[1], reverse=True)

        most_watched = "{} ({} ep), {} ({} ep), {} ({} ep)".format(
            # mw[0] == first/top tuple (genre, score), mw[2][1] == episodes watched for 3rd most watched genre
            GENRES_MAP[mw[0][0]],
            mw[0][1],
            GENRES_MAP[mw[1][0]],
            mw[1][1],
            GENRES_MAP[mw[2][0]],
            mw[2][1],
        )

        top_rated = "{} ({:.2f}), {} ({:.2f}), {} ({:.2f})".format(
            GENRES_MAP[tr[0][0]], tr[0][1], GENRES_MAP[tr[1][0]], tr[1][1], GENRES_MAP[tr[2][0]], tr[2][1]
        )

        success_print("Done computing stats!")
        custom_print(
            [
                (NO_STYLE, f"Anime stats ({self.mal_user})\n--\n"),
                (HEX_CYAN, "  Anime completed: "),
                (NO_STYLE, f"{completed}, watching {watching}.\n"),
                (HEX_CYAN, "  Total episodes watched: "),
                (NO_STYLE, f"{total_ep}\n"),
                (HEX_CYAN, "  Most watched: "),
                (NO_STYLE, f"{most_watched}\n"),
                (HEX_CYAN, "  Top rated (avg score): "),
                (NO_STYLE, f"{top_rated}\n"),
            ]
        )

    def display_empty_list(self):
        func = error_print if self.mal_user == self.USER_NOT_SET else print
        func(f"Anime list appears to be empty. User: {self.mal_user} (use 'connect' to reload or set the MAL user)")

    def display_list(self, args=None):
        """Display user's anime list"""
        if not self.mal_list:
            self.display_empty_list()
            return

        # Set defaults (all statuses, no genres excluded).
        included_status = self.LIST_STATUS_MAP.values()
        excluded_genres: List = []

        if args:
            options = extract_optional_args(args)
            for name, value in options:
                if name == "status":
                    if not value or type(value) != str:
                        error_print("Invalid value for 'status'. Check usage.")
                        return
                    splits = value.lower().split(",")
                    for split in splits:
                        if split not in self.LIST_STATUS_MAP.keys():
                            accepted = ", ".join(self.LIST_STATUS_MAP.keys())
                            error_print(
                                f"'status' must be a comma separated list, one or more of the following: {accepted}"
                            )
                            return
                    else:
                        # turn user inputs to status ids
                        included_status = [self.LIST_STATUS_MAP[name] for name in splits]

                elif name == "exclude":
                    if not value or type(value) != str:
                        error_print("Invalid value for 'exclude'. Check usage")
                        return
                    splits = value.lower().split(",")
                    for split in splits:
                        if split not in GENRES_MAP.values():
                            error_print(
                                "'exclude' must be a comma separated list of one or more anime genres (see usage)"
                            )
                            return
                    else:
                        excluded_genres = splits

                else:
                    error_print("Invalid option. Check usage with 'list --help'")
                    return

            if included_status != self.LIST_STATUS_MAP.values():
                included_str = ", ".join([WATCHING_STATUS[id] for id in included_status])
                warning_print(f"Filtering by status: {included_str}")

            if excluded_genres:
                msg = ", ".join(excluded_genres)
                warning_print(f"Excluding genres: {msg}")

                # switch to genre ids
                excluded_genres = [mal_id for mal_id, name in GENRES_MAP.items() if name in excluded_genres]

        # Copy anime list with reduced fields to simplify output
        working_list = []
        for anime in self.mal_list:
            if excluded_genres:
                # need generic anime to get the genre, check in cache first
                try:
                    generic = self._fetch_generic_anime(anime.mal_id)
                except APIException as err:
                    error_print(f"Error fetching anime {anime.mal_id} - {err}")
                    return
                else:
                    genres = [genre.mal_id for genre in generic.genres]
                    # check intersection between two lists
                    if list(set(genres) & set(excluded_genres)):
                        # skip the anime (excluded genre)
                        continue

            if anime.watching_status in included_status:
                working_list.append(
                    {
                        "title": anime.title,
                        "mal_id": anime.mal_id,
                        "watching_status": WATCHING_STATUS[anime.watching_status],
                        "score": anime.score,
                        "progress": f"{anime.watched_episodes} / {anime.total_episodes or '-'}",
                    }
                )

        print(f"Anime List -> MAL: {self.mal_user} (total post filtering: {len(working_list)})\n---")
        echo_via_pager(tabulate(working_list, headers="keys", tablefmt="github", stralign="left", numalign="left"))
        print()

    def search_anime(self, name: str, direct: bool = False):
        """Search for given anime, in user's list first then directly to MAL"""
        if self.mal_list and not direct:
            matches = [anime for anime in self.mal_list if cleanup_str(name) in cleanup_str(anime.title)]
            if matches:
                print(
                    f"Found {len(matches)} anime from {self.mal_user} MAL list. Try 'get <anime_id>' for more details"
                )
                # TODO - Use pager + table here, once figured out right format
                for anime in matches:
                    anime.display()
                return
            else:
                warning_print("No match in user's list. Searching on MAL...")

        # Direct search
        print("Searching...")
        try:
            response = self.api.search("anime", name.lower())
        except APIException as err:
            error_print(f"Error searching: '{err}'")
            return

        if len(response.get("results")) == 50:
            # TODO: Actually implement page argument, and prompt it here (--direct as well)
            warning_print("Note: Only the first 50 results returned by MAL are displayed.")

        results = [AnimeSearch(**raw) for raw in response["results"]]
        success_print(f"Search results for '{name}'\n")
        print("----")
        # TODO - Pager + Table
        for anime in results:
            anime.display()
        print()

    def get_anime(self, anime_id: str):
        """Attempts to retrieve the given anime full details (search by ID)"""
        try:
            anime_id = int(anime_id)
        except ValueError:
            print("Invalid argument: int expected")
            return

        # use cached version if available (max 7 days old)
        cached = self.cache.get(anime_id, max_days=7)
        if cached:
            cached.display()
            return

        try:
            anime = self.api.anime(anime_id)
        except APIException as err:
            if str(err).startswith("404"):
                error_print(f"Anime not found - id: {anime_id}")
            else:
                error_print(f"Error retrieving anime: {err}")
        else:
            anime = AnimeGeneric(**anime)
            self.cache.update(anime_id, anime)

            anime.display()

    def search_season(self, args: str):
        """Searches for all the anime released in a given season"""
        args = args.split()

        if len(args) != 2:
            error = "Invalid arguments: 2 expected. Check usage with --help."
        elif not str(args[0]).isnumeric():
            error = "Invalid argument: year. Integer expected"
        elif str(args[1]).lower() not in ["summer", "spring", "fall", "winter"]:
            error = "Invalid argument: second one should be one of 'summer', 'spring', 'fall', 'winter'"
        else:
            error = False

        if error:
            error_print(f"{error}")
            return
        else:
            year = int(args[0])
            season = str(args[1]).lower()
        try:
            response = self.api.season(year, season)
            results = [AnimeInSeason(**raw) for raw in response.get("anime")]
        except APIException as err:
            error_print(f"Error retrieving {season.capitalize()} {year}: '{err}'")
        else:
            # Copy anime list with reduced fields to simplify output
            working_list = []
            for anime in results:
                working_list.append(
                    (
                        {
                            "title": anime.title,
                            "episodes": anime.episodes or "-",
                            "score": anime.score or 0,
                            "genres": ", ".join([genre.name for genre in anime.genres]),
                            "mal_id": anime.mal_id,
                        }
                    )
                )

            print(f"Anime List for {season.capitalize()} {year} (total {len(working_list)})\n---")
            echo_via_pager(
                tabulate(
                    sorted(working_list, key=lambda k: k["score"], reverse=True),
                    headers="keys",
                    tablefmt="github",
                    stralign="left",
                    numalign="left",
                )
            )
            print()
