"""
Entry point. Run this to start the app and access the CLI.
"""

import argparse
import os.path

from prompt_toolkit import PromptSession
from prompt_toolkit.auto_suggest import AutoSuggestFromHistory
from prompt_toolkit.completion import WordCompleter
from prompt_toolkit.history import FileHistory

from animefinder import cache_dir
from animefinder.core import AnimeFinder


def run():
    parser = argparse.ArgumentParser(description="Run AnimeFinder app")
    parser.add_argument("--host", type=str, dest="host", help="Specify the Jikan REST URL", default=None)
    parser.add_argument("--cache", type=str, dest="cache", help="Custom path to JSON anime cache", default=None)
    args = parser.parse_args()

    finder = AnimeFinder(host=args.host, cache_path=args.cache)
    cmd_completer = WordCompleter(words=finder.handler.commands.keys(), ignore_case=True)

    cmd_history_path = os.path.join(cache_dir, "history.txt")
    session = PromptSession("animefinder> ", history=FileHistory(cmd_history_path))

    while True:
        try:
            text = session.prompt(auto_suggest=AutoSuggestFromHistory(), completer=cmd_completer).strip()
            if text:
                if text == "quit" or text == "exit" or text == "bye":
                    finder.terminate()
                    break
                else:
                    finder.handler.process(text)
        except KeyboardInterrupt:
            continue
        except EOFError:
            break


if __name__ == "__main__":
    run()
