import os
import pathlib

# setup global path/objects and logging
home_dir = os.path.abspath(pathlib.Path().home())
root_dir = os.path.join(home_dir, ".animefinder")
cache_dir = os.path.join(root_dir, "cache")
logs_dir = os.path.join(root_dir, "logs")

for app_dir in [root_dir, cache_dir, logs_dir]:
    if not os.path.exists(app_dir):
        os.makedirs(app_dir)
