import re
import string

import prompt_toolkit.shortcuts

from prompt_toolkit import print_formatted_text as color_print
from prompt_toolkit.formatted_text import FormattedText

# Hex colors for printing
HEX_YELLOW = "#ffc61a"
HEX_GREEN = "#33cc33"
HEX_RED = "#ff0000"
HEX_CYAN = "#00ffcc"
NO_STYLE = ""

# Regex for stripping punctuation
regex = re.compile(f"[{re.escape(string.punctuation)}]")


def cleanup_str(text: str) -> str:
    """Returns given string turned to lowercase and with punctuation removed"""
    clean = text.lower()
    return regex.sub(" ", clean)


def clear_screen():
    prompt_toolkit.shortcuts.clear()


def error_print(text: str):
    color_print(FormattedText([(HEX_RED, text)]))


def success_print(text: str):
    color_print(FormattedText([(HEX_GREEN, text)]))


def warning_print(text: str):
    color_print(FormattedText([(HEX_YELLOW, text)]))


def custom_print(lines):
    color_print(FormattedText(lines))
