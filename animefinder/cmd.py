from typing import List, Tuple


class Option:
    def __init__(self, name, description):
        self.name = name
        self.desc = description

    def print_usage(self):
        print(f"--{self.name:<16}{self.desc}")


class Command:
    def __init__(self, name, usage, description, func, args=False):
        self.name = name
        self.usage = usage
        self.desc = description
        self.args = args
        self.run = func
        self.options: List[Option] = []

    def print_usage(self):
        print(f"Usage: {self.usage}\n\n  {self.desc}\n")
        if self.options:
            print("Options:")
            for option in self.options:
                option.print_usage()

    def add_option(self, option: Option):
        self.options.append(option)


class CommandHandler:
    def __init__(self, not_found: str = "Unknown command"):
        self.commands = {}
        self.not_found = not_found

    def register(self, cmd: Command):
        self.commands[cmd.name] = cmd

    def process(self, text: str):
        splits = text.strip().split()
        name = splits[0].lower()
        args = splits[1:]

        cmd: Command = self.commands.get(name)
        if not cmd:
            print(self.not_found)
            return

        if cmd.args and not args:
            print(f"Missing arguments. Run '{cmd.name} --help' to see usage info")
            return

        if args:
            if args[0] == "--help":
                cmd.print_usage()
                return
            # commenting this out to allow commands to have optional arguments
            # elif not cmd.args:
            #     print(f"Too many arguments. Run '{cmd.name} --help' to see usage info")
            #     return
            else:
                cmd.run(" ".join(args))
        else:
            cmd.run()


def extract_optional_args(args) -> List[Tuple]:
    if type(args) == str:
        args: List[str] = args.split()

    optionals: List[Tuple] = []
    for split in args:
        if split.startswith("--") and len(split) > 3:
            name = split.removeprefix("--").split("=")[0]
            try:
                value = split.split("=")[1]
            except IndexError:
                value = True
            if name:
                optionals.append((name, value))
    return optionals
