"""
Depending on the context, MAL/Jikan return different fields for an 'Anime' object.

For example, when iterating through a user's anime list:
    - score means the one given by the user (as opposed to overall anime score)
    - no 'synopsis', 'genres', 'related', etc.

Same thing when getting the anime through a genre search, direct search, or by ID.

The different models to be considered are represented here.

"""

from datetime import datetime
from typing import Dict, List

from pydantic import BaseModel

from animefinder.utils import HEX_GREEN, NO_STYLE, custom_print


class AnimeBase(BaseModel):
    """Fields found in all anime objects, no matter the endpoint"""

    mal_id: int
    url: str
    title: str
    image_url: str
    type: str
    score: float = None


class AnimeInList(AnimeBase):
    """This models a Jikan Anime object when getting it from the user's anime list."""

    score: int
    video_url: str
    watching_status: int
    watched_episodes: int
    total_episodes: int
    tags: str = None
    airing_status: int
    season_name: str = None
    season_year: int = None
    rating: str = None
    start_date: datetime = None
    end_date: datetime = None
    watch_start_date: datetime = None
    watch_end_date: datetime = None
    days: int = None

    def display(self):
        """Display this anime (highlight keywords)"""
        custom_print(
            [
                (NO_STYLE, "{\n\t"),
                (HEX_GREEN, "title: "),
                (NO_STYLE, f"{self.title:<60}\n\t"),
                (HEX_GREEN, "id: "),
                (NO_STYLE, f"{self.mal_id}\n\t"),
                (HEX_GREEN, "year: "),
                (NO_STYLE, f"{self.season_year}\n\t"),
                (HEX_GREEN, "score: "),
                (NO_STYLE, f"{self.score}\n\t"),
                (HEX_GREEN, "ep: "),
                (NO_STYLE, f"{self.total_episodes}\n\t"),
                (HEX_GREEN, "url: "),
                (NO_STYLE, f"{self.url:<60}\n\t"),
                (NO_STYLE, "}\n"),
            ]
        )


class AnimeSearch(AnimeBase):
    """Anime object returned via Search endpoint"""

    airing: bool
    synopsis: str
    episodes: str
    start_date: datetime = None
    end_date: datetime = None
    members: int
    rated: str = None

    def display(self):
        """Display this anime (highlight keywords)"""
        year = self.start_date.year if self.start_date else "n/a"
        custom_print(
            [
                (NO_STYLE, "{\n\t"),
                (HEX_GREEN, "title: "),
                (NO_STYLE, f"{self.title:<60}\n\t"),
                (HEX_GREEN, "id: "),
                (NO_STYLE, f"{self.mal_id}\n\t"),
                (HEX_GREEN, "year: "),
                (NO_STYLE, f"{year}\n\t"),
                (HEX_GREEN, "score: "),
                (NO_STYLE, f"{self.score}\n\t"),
                (HEX_GREEN, "ep: "),
                (NO_STYLE, f"{self.episodes}\n\t"),
                (HEX_GREEN, "members: "),
                (NO_STYLE, f"{self.members}\n\t"),
                (HEX_GREEN, "url: "),
                (NO_STYLE, f"{self.url:<60}\n\t"),
                (HEX_GREEN, "synopsis: "),
                (NO_STYLE, f"'{self.synopsis}'\n"),
                (NO_STYLE, "}\n"),
            ]
        )


class Genre(BaseModel):
    """Defines an anime genre"""

    mal_id: int
    type: str
    name: str
    url: str


class AnimeInGenre(AnimeBase):
    """Anime object returned via Genre endpoint"""

    synopsis: str
    airing_start: datetime = None
    episodes: int = None
    members: int
    genres: List[Genre]


class Producer(BaseModel):
    mal_id: int
    type: str
    name: str
    url: str


class AnimeInSeason(AnimeBase):
    """Anime object returned via Season endpoint"""

    score: float = None
    synopsis: str
    airing_start: datetime = None
    episodes: int = None
    members: int
    genres: List[Genre]
    source: str
    producers: List[Producer]
    licensors: List[str]
    r18: bool
    kids: bool
    continuing: bool


class AiredTime(BaseModel):
    from_: datetime = None
    to: datetime = None

    # this is necessary to use the python reserved keyword 'from' as a field name
    class Config:
        fields = {"from_": "from"}


class AnimeGeneric(AnimeBase):
    """Anime object returned via Anime endpoint"""

    synopsis: str = None
    airing: bool
    members: int
    episodes: int = None
    genres: List[Genre]
    aired: AiredTime
    score: float = None

    def display(self):
        """Display this anime (highlight keywords)"""
        custom_print(
            [
                (NO_STYLE, "{\n\t"),
                (HEX_GREEN, "title: "),
                (NO_STYLE, f"{self.title:<60}\n\t"),
                (HEX_GREEN, "id: "),
                (NO_STYLE, f"{self.mal_id}\n\t"),
                (HEX_GREEN, "year: "),
                (NO_STYLE, f"{self.aired.from_.year}\n\t"),
                (HEX_GREEN, "score: "),
                (NO_STYLE, f"{self.score}\n\t"),
                (HEX_GREEN, "ep: "),
                (NO_STYLE, f"{self.episodes}\n\t"),
                (HEX_GREEN, "members: "),
                (NO_STYLE, f"{self.members}\n\t"),
                (HEX_GREEN, "url: "),
                (NO_STYLE, f"{self.url:<60}\n\t"),
                (HEX_GREEN, "synopsis: "),
                (NO_STYLE, f"'{self.synopsis}'\n"),
                (NO_STYLE, "}\n"),
            ]
        )


# Anime watching status (mapping MAL values to readable strings)
WATCHING_STATUS: Dict[int, str] = {1: "Watching", 2: "Completed", 3: "On Hold", 4: "Dropped", 6: "Plan to Watch"}

#  Mapping MAL/Jikan Anime genres ID to readable strings
GENRES_MAP: Dict[int, str] = {
    1: "action",
    2: "adventure",
    3: "cars",
    4: "comedy",
    5: "dementia",
    6: "demons",
    7: "mystery",
    8: "drama",
    9: "ecchi",
    10: "fantasy",
    11: "game",
    12: "hentai",
    13: "historical",
    14: "horror",
    15: "kids",
    16: "magic",
    17: "martial_arts",
    18: "mecha",
    19: "music",
    20: "parody",
    21: "samurai",
    22: "romance",
    23: "school",
    24: "sci_fi",
    25: "shoujo",
    26: "shoujo_ai",
    27: "shounen",
    28: "shounen_ai",
    29: "space",
    30: "sports",
    31: "super_power",
    32: "vampire",
    33: "yaoi",
    34: "yuri",
    35: "harem",
    36: "slice_of_life",
    37: "supernatural",
    38: "military",
    39: "police",
    40: "psychological",
    41: "thriller",
    42: "seinen",
    43: "josei",
    47: "gourmet",
}
