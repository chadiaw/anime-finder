import json
import os
from datetime import datetime
from typing import Dict, Union

import jsonpickle
from pydantic import BaseModel

from animefinder import cache_dir
from animefinder.model import AnimeGeneric


class CacheRecord(BaseModel):
    anime: AnimeGeneric
    timestamp: datetime


class AnimeCache:
    def __init__(self, cache_path: str = None):
        """
        Initializes and loads the cache from the given path (or use default)
        """
        if not cache_path:
            cache_path = os.path.join(cache_dir, "anime_cache.json")

        self.path: str = cache_path
        self.cache: Dict[str, CacheRecord] = self.load()

    def load(self) -> Dict[str, CacheRecord]:
        """
        Read cache from the JSON file.
        Returns a new dictionary if file not found/can't decode.
        """
        try:
            with open(self.path, "r") as file:
                cache_str = file.read()
                return jsonpickle.decode(cache_str)

        except (FileNotFoundError, json.JSONDecodeError):
            return {}

    def save(self):
        """
        Persist the current cache to the JSON file
        """
        with open(self.path, "w") as file:
            frozen = jsonpickle.encode(self.cache)
            file.write(frozen)

    def update(self, key: Union[str, int], anime: AnimeGeneric):
        """
        Add/Update an anime in the current cache
        :param key: anime id
        :param anime: anime object
        """
        key = str(key)
        self.cache[key] = CacheRecord(anime=anime, timestamp=datetime.now())

    def get(self, key: Union[str, int], max_days: int = 30):
        """
        Retrieve anime from the cache, if it exists (None otherwise)

        :param key: anime id
        :param max_days: limit how many days ago the anime was refreshed (default 30 days)
        :return: Anime if found
        """
        key = str(key)
        if key not in self.cache.keys():
            return None
        record: CacheRecord = self.cache.get(key)
        diff = record.timestamp - datetime.now()
        if diff.days < max_days:
            return record.anime
        else:
            return None
