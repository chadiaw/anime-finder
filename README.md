[![pipeline status](https://gitlab.com/chadiaw/anime-finder/badges/master/pipeline.svg)](https://gitlab.com/chadiaw/anime-finder/-/commits/master) [![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
 [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![security: bandit](https://img.shields.io/badge/security-bandit-green.svg)](https://github.com/PyCQA/bandit)

# Anime Finder

This is a command line application (CLI) for tracking and finding anime (and other things such as stats, data analysis, etc.)

Concept is the same as services such as [MyAnimeList (MAL)](https://myanimelist.net/), which we'll depend on to get most of the data - via [Jikan API](https://jikan.moe/)

Difference is we're doing it all from the command line and python, because it's fun and this project allows me to experiment with a bunch of python things as well.

[![asciicast](https://asciinema.org/a/310853.png)](https://asciinema.org/a/310853)

## Usage

Package management is handled using [poetry](https://python-poetry.org/).

- Clone repo, cd to directory
- Run `poetry install --no-dev` (remove option if you need the dev dependencies)

To run the app, run `animefinder` and you should get the prompt to start typing commands.

To exit the app type quit, exit, bye or press CTRL+D

 To use a self-hosted version of Jikan (explained below), pass the URL while starting the app, like in the following example:
````
animefinder --host 'http://localhost:9001/public/v3'
````

## Dev environment

The default poetry install should have installed all dev dependencies.

The file `.pre-commit-config.yaml` specifies a bunch of checks to run before committing code. This is automated using [pre-commit](https://pre-commit.com/). It should already be installed if you used `--dev`.

After the poetry install, run ``pre-commit install`` in the [virtual environment](https://python-poetry.org/docs/basic-usage/#using-your-virtual-environment) to set up the git hook scripts. Now pre-commit will run automatically on every commit, and fix things if necessary.

At any time you can also run ``pre-commit run --all-files`` to check all your files against the set hooks.

## Jikan

### What's Jikan?

MyAnimeList is arguably one of the biggest anime databases out there, but has* no API, meaning the best way to get data out of it is to scrape the website.

That is exactly what Jikan does, scrape the website and offer an API interface, making it [MAL unofficial API](https://github.com/jikan-me/jikan).

> MAL official API (v2) entered open beta in July 2020 -> [Official MAL API - Club](https://myanimelist.net/clubs.php?cid=13727)

### Using Jikan

- There is a public REST API (see https://jikan.docs.apiary.io/#) that anyone can use to make requests (straight from Postman, curl or any REST client)
- There are [wrappers](https://github.com/jikan-me/jikan-rest#wrappers) in a variety of languages, which make the same requests but wrapped in objects and constructs easier to work with (using the Python one for this project, jikanpy)
- You can also self-host the [project](https://github.com/jikan-me/jikan-rest), e.g. using the [docker image](https://github.com/jikan-me/jikan-docker).
